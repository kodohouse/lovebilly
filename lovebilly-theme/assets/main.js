jQuery(document).ready(function($){

    var Cart = {

        init: function () {
            Cart.updateCartGeneral();
            Cart.updateQuantity();
            Cart.removeItem();
            Cart.calculateTaxes();
            $('main.product').length && Cart.addItemToCart();
        },

        addItemToCart: function () {
            var $button = $('.button--cart');

            $button.on('click', function () {
                setAdding();
                addToCart();
            });

            function setAdding() {
                $button.addClass('button--adding');
                var $message = $button.find('.button__message');
                var width = $message.outerWidth();
                $message.css('min-width', width);
            }

            function setAdded() {
                $button.removeClass('button--adding').addClass('button--added');
                setTimeout(function () {
                    $button.removeClass('button--added').blur();
                }, 1000);
                Cart.updateCartGeneral();
            }

            function addToCart() {
                var q = $button.attr('data-cart-quantity');
                if ($button.hasClass('add-to-cart-personalized')) {
                    var id = $button.attr('data-custom-variant');
                    var front = $button.attr('data-custom-front');
                    var back = $button.attr('data-custom-back');
                    var $frontInput = $('#personalize-front');
                    var $backInput = $('#personalize-back');
                    CartJS.addItem(id, q, {
                        "Front": front,
                        "Back": back
                    },
                    {
                        success: function(data, textStatus, jqXHR) {
                            setAdded();
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log('Error: ' + errorThrown + '!');
                        }
                    });
                } else {
                    var id = $button.attr('data-variant-id');
                    CartJS.addItem(id, q, {

                    },
                    {
                        success: function(data, textStatus, jqXHR) {
                            setAdded();
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log('Error: ' + errorThrown + '!');
                        }
                    });
                }
            }
        },

        updateCartGeneral: function () {
            jQuery.getJSON('/cart.js', function(data) {
                $('.header__nav-bag span').html(data.item_count);
                $('.cart__subtotal .value').html(Shopify.formatMoney(data.total_price));
            });
        },

        updateQuantity: function () {
            $('.item__update').on('click', function (e) {
                e.preventDefault();
                var item = $(this).closest('.item');
                var id = item.attr('id');
                var quantity = item.find('.count__number').val();
                var itemTotal = item.find('.value-total .item-total');
                var newItemTotal = itemTotal.attr('data-price') * quantity;
                CartJS.updateItemById(id, quantity, {}, {success: function (data) {
                    itemTotal.html(Shopify.formatMoney(newItemTotal));
                    Cart.updateCartGeneral();
                }});
            });
        },

        removeItem: function () {
            $('.item__remove').on('click', function (e) {
                e.preventDefault();
                var item = $(this).closest('.item');
                var id = item.attr('id');
                CartJS.updateItemById(id, 0, {}, {success: function () {
                    Cart.updateCartGeneral();
                }});
                item.addClass('item--byebye');
                setTimeout(function () {
                    item.slideUp(333);
                    if ($('.item:not(.item--byebye)').length == 0) {
                        location.reload();
                    }
                }, 500);
            });
        },

        calculateTaxes: function () {
            var country, state;
            $('.cbb-shipping-rates-rate-amount').waitUntilExists(function () {

            });
        }
    }

    var Product = {
        init: function () {
            Product.countProduct();
            Product.selectVariants();
            $('.product__notes').length && Product.stickyNotes();
        },

        stickyNotes: function () {
            var notes = $('.product__info');
            var windowH = $(window).height();
            var scrolled = false;
            $(window).on('scroll', function () {
                if ($(window).width() >= 1200) {
                    var notesH = notes.height();
                    var footerTop = $('.footer').offset().top;
                    if ($('.exico').length) {
                        footerTop = $('.exico').offset().top;
                    }
                    var scroll = $(window).scrollTop();
                    var x = 280 + notesH - windowH;
                    if (scroll > 210) {
                        notes.css({'position' : 'fixed', 'top' : 70, 'bottom' : 'initial'});
                        if (scroll > footerTop - notesH - 190) {
                            notes.css({'position' : 'absolute', 'bottom' : 85, 'top' : 'initial'});
                        }
                    } else {
                        notes.css({'position' : 'absolute', 'top' : 0, 'bottom' : 'initial'});
                    }
                }
            });
        },

        selectVariants: function () {
            function updateSelection() {
                var product = $(this).closest('.product');
                var variants = $(this).siblings().removeClass('active');
                var selectedValue = $(this).addClass('active').attr('data-value').trim();
                product
                .find('option[value="' + selectedValue + '"]')
                .prop('selected', true)
                .closest('select')
                .change();
            }
            $('.product__variants .option li').on('click', updateSelection);
        },

        countProduct: function () {
            $('.count span').on('click', function() {
                var input = $(this).parent().find('.count__number');
                if ($(this).hasClass('count__minus')) {
                    decrementCartCounter(input);
                } else if ($(this).hasClass('count__plus')) {
                    var inventory = input.attr('data-inventory') || 100;
                    if (input.val() < inventory) {
                        incrementCartCounter(input);
                    }
                }
            });

            function incrementCartCounter(input) {
                var count = parseInt(input.val()) || 0;
                count++;
                updateCounter(input, count);
            }

            function decrementCartCounter(input) {
                var count = parseInt(input.val()) || 0;
                count--;
                updateCounter(input, count);
            }

            function updateCounter(input, count) {
                if (count >= 1) {
                    if (count == 1000) {
                        return;
                    }
                    input.attr('value', count);

                } else {
                    input.attr('value', 1);
                }
                $(input)
                .closest('.product')
                .find('.add-to-cart')
                .attr('data-cart-quantity', count);
            }
        }
    }

    var Site = {

        init: function() {
            Site.customFunction();
            Site.navFunctions();
            Site.initSliders();
            Site.initParallax();
            Site.toggleSearch();
            Site.toggleFaq();
            Site.editAccount()
            Site.scrollToElement();
            Site.socialShare();
        },

        customFunction: function() {
            console.log('RIOT.agency');
        },

        socialShare: function () {
            $('.share-fancy').on('click', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                window.open(url, "_blank", "width=440,height=456,menubar=no,toolbar=no");
            })
        },

        navFunctions: function () {
            var navBtn = document.getElementsByClassName('header__nav-btn')[0];
            navBtn.addEventListener('click', toggleMainNav);

            function toggleMainNav() {
                var header = document.getElementsByClassName('header')[0];
                header.classList.toggle('mobile-nav-open');
                $('body').toggleClass('mobile-nav-open');
                $('.header__overlay').toggleClass('header__overlay--active');
            }

            $('.header__nav .is-subnav').on('click', function (e) {
                e.preventDefault();
                if (!$(this).hasClass('active')) {
                    $(this).next().slideToggle().toggleClass('active');
                    var link = $(this).attr('href');
                }
            });
        },

        initSliders: function() {
            var autoplay = $('.homepage__slider .slide').data('autoplay');
            var autoplaySpeed = $('.homepage__slider .slide').data('autoplay-speed');
            $('.homepage__slider').slick({
                arrows: false,
                dots: true,
                swipe: false,
                fade: true,
                speed: 900,
                autoplay: autoplay,
                autoplaySpeed: autoplaySpeed
            });

            if ($(window).width() <= 1024) {
                var headerH = $('.homepage__slider').height();
                $('.homepage__slider').css('max-height', headerH);
            }

            $('.product__gallery--mobile').slick({
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><</button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next">></button>',
                infinite: true
            });
        },

        initParallax: function () {
            var rellax = new Rellax('.rellax:not(.product__image)');
        },

        toggleSearch: function () {
            function showSearchBox() {
                $('.header__overlay').addClass('header__overlay--active header__overlay--search');
                $('.search-box').addClass('search-box--active');
                $('.search-box .input--search').focus();
                $('.header').addClass('header--overlay')
                $('.search-box .input--search').val('');
                setTimeout(function () {
                    $('.search-box .input--search').focus();
                }, 100);
            }

            function hideSearchBox() {
                $('.header__overlay').removeClass('header__overlay--active header__overlay--search');
                $('.search-box').removeClass('search-box--active');
                $('.header').removeClass('header--overlay')
            }

            function clearSearchBox() {
                $('.search-box .input--search').val('').focus();
            }
            $('.header__nav-search').on('click', showSearchBox);

            $('.search-box__clear').on('click', hideSearchBox);

            $(document).on('keyup',function(evt) {
                if (evt.keyCode == 27) {
                    hideSearchBox();
                }
            });
        },

        toggleFaq: function () {
            $('.page--faq h3').on('click', function () {
                $(this).toggleClass('active');
                $(this).next('div').slideToggle();
            })

        },

        editAccount: function () {
            function toggleSections() {
                if (!$(this).hasClass('active')) {
                    $('.account__nav h2, .account__orders, .account__addresses').toggleClass('active');
                }
            }

            function toggleNewAddress() {
                $('.add-new-address span, #AddAddress').toggle();
            }

            function toggleEditAddress() {
                $(this).toggleClass('active');
                var form = $(this).closest('.address').find('.form').toggle();

            }

            function deleteAddress() {
                var addressId = $(this).closest('.address').attr('id');
                Shopify.postLink(
                    '/account/addresses/' + addressId,
                    {'parameters': {'_method': 'delete'}}
                );
            }

            function showErrors() {
                $('.form .errors').closest('.form').show();
                $('.add-new-address span').toggle();
            }

            $('.address__buttons [href="#edit"]').on('click', toggleEditAddress);
            $('.address__buttons [href="#delete"]').on('click', deleteAddress);
            $('.add-new-address span').on('click', toggleNewAddress);
            $('.form .errors').length > 0 && showErrors();
        },

        scrollToElement: function () {
            $('.blog__header .scroll-down').click(function() {
                scrollTo($('.blog .article:first-of-type'), 666);
            });

            function scrollTo(el, speed) {
                $('html, body').animate({
                    scrollTop: el.offset().top
                }, speed);
            }
        },

        joinMailing: function () {
            var button = $('.mailing__button');
            var input = $('.mailing__form input');
            var mailing = $('.mailing')
            input.on('keyup', function () {
                mailing.addClass('mailing--valid');
                if (validateEmail($(this))) {
                    mailing.addClass('mailing--valid');
                } else {
                    mailing.removeClass('mailing--valid');
                }
            });
        }

    };

    // here we go!
    Site.init();
    Cart.init();
    Product.init();
});

;(function ($, window) {

    var intervals = {};
    var removeListener = function(selector) {

        if (intervals[selector]) {

            window.clearInterval(intervals[selector]);
            intervals[selector] = null;
        }
    };
    var found = 'waitUntilExists.found';

    /**
    * @function
    * @property {object} jQuery plugin which runs handler function once specified
    *           element is inserted into the DOM
    * @param {function|string} handler
    *            A function to execute at the time when the element is inserted or
    *            string "remove" to remove the listener from the given selector
    * @param {bool} shouldRunHandlerOnce
    *            Optional: if true, handler is unbound after its first invocation
    * @example jQuery(selector).waitUntilExists(function);
    */

    $.fn.waitUntilExists = function(handler, shouldRunHandlerOnce, isChild) {

        var selector = this.selector;
        var $this = $(selector);
        var $elements = $this.not(function() { return $(this).data(found); });

        if (handler === 'remove') {

            // Hijack and remove interval immediately if the code requests
            removeListener(selector);
        }
        else {

            // Run the handler on all found elements and mark as found
            $elements.each(handler).data(found, true);

            if (shouldRunHandlerOnce && $this.length) {

                // Element was found, implying the handler already ran for all
                // matched elements
                removeListener(selector);
            }
            else if (!isChild) {

                // If this is a recurring search or if the target has not yet been
                // found, create an interval to continue searching for the target
                intervals[selector] = window.setInterval(function () {

                    $this.waitUntilExists(handler, shouldRunHandlerOnce, true);
                }, 500);
            }
        }

        return $this;
    };

}(jQuery, window));

function validateEmail($input) {
    var email = $input.val();
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,7}$/;
    if (filter.test(email)) {
        return true;
        $input.removeClass('input-error')
    } else {
        $input.addClass('input-error')
        return false;
    }
}
