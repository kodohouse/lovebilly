$( function() {

    var personalizeInputs = $('input#personalize-front, input#personalize-back');
    // var personalizeImage = $( '.product__image--personalized');
    var personalizeImageFront = $( '.product__image--front');
    var personalizeImageBack = $( '.product__image--back');

    personalizeInputs.on('focus', function( e ) {

        var letter = $(this).val()
        if (letter) {
            tagLetter(letter, $(this));
        }

    } );

    // Filter out keys that the user shouldn't be allowed to type
    personalizeInputs.on( 'keydown', function(e) {

        // on iphone e.key is undefined so we will just delete a bad character on keyup
        if (e.key) {
            var text = e.key;
            var filteredText = text.replace(/[^a-zA-Z0-9]/gi, '');

            if (text.length > 1) {
                return; // this is some kind of control key or not a letter
            }

            if ( text.length != filteredText.length ) {
                e.preventDefault(); // this is a non-allowed character

            }
        }
    });

    // Handle monogram text change
    personalizeInputs.on( 'keyup', function(e) {
        var text = $(this).removeClass('shake-it').val();
        tagLetter(text, $(this))
    } );

    function tagLetter(letter, input) {
        var text = letter;
        var filteredText = text.replace(/[^a-zA-Z0-9]/gi, '');

        if ( text.length != filteredText.length || text.length > 1) {
            // In case using mobile safari or wasn't caught on keydown. Delete value do nothing
            input.val('');
            return;
        }
        else if ( text.length > 1) {
            // just a control key, probably mobile safari. Do nothing
            return;
        }

        else {
            // whaaaat
            var personalizeImage;
            var button = $('.button--cart');
            if (input.attr('id') == 'personalize-front') {
                personalizeImage = personalizeImageFront;
                console.log(button, text);
                button.attr('data-custom-front', text.toUpperCase());
            } else {
                personalizeImage = personalizeImageBack;
                button.attr('data-custom-back', text.toUpperCase());
            }

            var personalizeOverlay = personalizeImage.find('.personalized-letter')
            filteredText = filteredText.toLowerCase();

            if ( filteredText.length == 0 ) {
                personalizeOverlay.css({
                    'background-image': 'none'
                });
            } else {

                // for the record, record uppercase letter for submit
                if (text != text.toUpperCase() ) {
                    input.val(text.toUpperCase());
                }

                var tagURL = personalizeOverlay.data('url');
                tagURL = tagURL.replace('tag-a', 'tag-' + filteredText);

                personalizeOverlay.css({
                    'background-image': 'url("' + tagURL + '")'
                });
            }
        }
    }


    // LETTER PRELOAD
    // We should preload the letters so they are cached and available immediately on the page
    //
    $(window).load(function(){
        window.letterImages = [];
        var tagURL = $('.personalized-letter').data('url');

        // 0-9
        for(i=48;i<=57;i++){
            var image = new Image();
            var letter = String.fromCharCode(i);
            newTagURL = tagURL.replace('tag-a', 'tag-' + letter);
            image.src = newTagURL;
            window.letterImages.push(image);
            // console.log('Loading %s', letter);
        }
        // a-z
        for(i=97;i<=122;i++){
            var image = new Image();
            var letter = String.fromCharCode(i);
            newTagURL = tagURL.replace('tag-a', 'tag-' + letter);
            image.src = newTagURL;
            window.letterImages.push(image);
            // console.log('Loading %s', letter);
        }

    });

} );
